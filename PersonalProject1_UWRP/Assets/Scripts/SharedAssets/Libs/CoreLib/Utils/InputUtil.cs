﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace SharedAssets.Libs.CoreLib.Utils
{
    public class InputUtil : MonoBehaviour
    {
        public static bool IsTouchBegan
        {
            get
            {
                return Input.GetMouseButtonDown(0) || Input.touchCount == 1 && Input.GetTouch(0).phase == TouchPhase.Began;
            }
        }
        
        public static bool IsTouchEnded
        {
            get
            {
                return Input.GetMouseButtonUp(0) || Input.touchCount == 1 && Input.GetTouch(0).phase == TouchPhase.Ended | Input.GetTouch(0).phase == TouchPhase.Canceled;
            }
        } 
        
        public static bool IsTouch
        {
            get
            {
                return Input.GetMouseButton(0) || Input.touchCount == 1 && Input.GetTouch(0).phase == TouchPhase.Moved;
            }
        } 
        
        public static Vector2 TouchPosition
        {
            get
            {
                if (Input.touchCount == 1)
                {
                    return Input.GetTouch(0).position;
                }

                return Input.mousePosition;
            }
        }
        
        public static bool IsPointerOverGameObject
        {
            get
            {
                return EventSystem.current.IsPointerOverGameObject() || Input.touchCount == 1 && EventSystem.current.IsPointerOverGameObject(Input.GetTouch(0).fingerId);
            }
        }
    }
}