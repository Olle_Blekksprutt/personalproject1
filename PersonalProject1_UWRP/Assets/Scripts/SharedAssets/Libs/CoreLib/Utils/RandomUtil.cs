using System.Collections.Generic;
using UnityEngine;

namespace SharedAssets.Libs.CoreLib.Utils
{
    public static class RandomUtil
    {
        public static object GetRandomFrom(List<ObjectAndChanceStrength> objectAndChanceStrengthCollection)
        {
            var totalStrength = 0f;
            objectAndChanceStrengthCollection.ForEach(o => totalStrength += o.ChanceStrength);

            var randomStrength = Random.Range(0, totalStrength);
            var strength = 0f;
            var objectAndChanceStrength = objectAndChanceStrengthCollection.Find(o =>
            {
                strength += o.ChanceStrength;
                return strength >= randomStrength;
            });
            
            return objectAndChanceStrength.Object;
        }

        
        public class ObjectAndChanceStrength
        {
            public object Object { get; private set; }
            public float ChanceStrength { get; private set; }


            public ObjectAndChanceStrength(object obj, float chanceStrength)
            {
                Object = obj;
                ChanceStrength = chanceStrength;
            }
        }
    }
}