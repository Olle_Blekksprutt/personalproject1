﻿using System;
using System.Collections;
using UnityEngine;

namespace SharedAssets.Libs.CoreLib.Utils
{
    public static class CoroutinesUtil
    {
        public static void CallAfter(float seconds, MonoBehaviour coroutineHolder, Action action)
        {
            coroutineHolder.StartCoroutine(CallAfterCoroutine(seconds, action));
        }
        
        public static void CallAfterFrames(int numFrames, MonoBehaviour coroutineHolder, Action action)
        {
            coroutineHolder.StartCoroutine(CallAfterFramesCoroutine(numFrames, action));
        }


        private static IEnumerator CallAfterCoroutine(float seconds, Action action)
        {
            yield return new WaitForSeconds(seconds);
            action();            
        }
        
        private static IEnumerator CallAfterFramesCoroutine(int numFrames, Action action)
        {
            for (var i = 0; i < numFrames; i++)
            {
                yield return null;
            }            
            action();            
        }
    }
}