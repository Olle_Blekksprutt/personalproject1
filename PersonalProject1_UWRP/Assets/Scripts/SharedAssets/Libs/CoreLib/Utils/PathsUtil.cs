﻿using System.IO;

namespace SharedAssets.Libs.CoreLib.Utils
{
    public static class PathsUtil
    {
        public static void CreateDirectoryIfNotExist(string path)
        {
            if (Directory.Exists(path) == false)
            {
                Directory.CreateDirectory(path);
            }
        }
    }
}