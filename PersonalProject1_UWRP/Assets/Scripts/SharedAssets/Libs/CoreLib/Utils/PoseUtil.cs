﻿using UnityEngine;

namespace SharedAssets.Libs.CoreLib.Utils
{
    public static class PoseUtil
    {
        public static Pose GetPose(Transform transform)
        {
            return new Pose(transform.position, transform.rotation);
        }        
        
        public static void SetPose(Transform transform, Pose pose)
        {
            transform.position = pose.position;
            transform.rotation = pose.rotation;
        } 
        
        public static Pose GetLocalPose(Transform transform)
        {
            return new Pose(transform.localPosition, transform.localRotation);
        }
        
        public static void SetLocalPose(Transform transform, Pose pose)
        {
            transform.localPosition = pose.position;
            transform.localRotation = pose.rotation;
        }
    }
}