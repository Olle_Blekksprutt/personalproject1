﻿using UnityEngine;

namespace SharedAssets.Libs.CoreLib.Utils
{
    public static class PhysicsUtil
    {
        public static bool IsRaycastHitBoundTo(RaycastHit raycastHit, GameObject gameObject)
        {
            return IsColliderBoundTo(raycastHit.collider, gameObject);
        }
        
        public static bool IsColliderBoundTo(Collider collider, GameObject gameObject)
        {
            var colliderGameObject = collider.gameObject;
            return colliderGameObject == gameObject || colliderGameObject.transform.IsChildOf(gameObject.transform);
        }
    }
}