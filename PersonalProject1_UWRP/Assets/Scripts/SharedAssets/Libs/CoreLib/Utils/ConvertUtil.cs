using System;

namespace SharedAssets.Libs.CoreLib.Utils
{
    public static class ConvertUtil
    {
        public static T ConvertToEnum<T>(string value)
        {
            return (T) Enum.Parse(typeof(T), value);
        }
    }
}