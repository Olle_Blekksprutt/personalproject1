﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SharedAssets.Libs.CoreLib.Modules.Configuration.Data
{
    [CreateAssetMenu(fileName = "GameplayConfiguration", menuName = "SharedAssets/Configurations")]
    public class GameplayConfiguration : ScriptableObject
    {

    }
}

