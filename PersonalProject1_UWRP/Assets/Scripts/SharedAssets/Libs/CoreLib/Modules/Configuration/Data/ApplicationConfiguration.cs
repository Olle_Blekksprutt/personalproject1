﻿using System;

namespace SharedAssets.Libs.CoreLib.Modules.Configuration.Data
{
    [Serializable]
    public class ApplicationConfiguration
    {
        public int ScreenWidth;
        public int ScreenHeight;
    }

}
