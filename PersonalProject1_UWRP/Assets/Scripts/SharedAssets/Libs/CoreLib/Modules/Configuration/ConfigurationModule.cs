﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SharedAssets.Libs.CoreLib.Modules.Configuration.Data;
using SharedAssets.Libs.CoreLib.Modules.Configuration.ConfigurationLib;
using SharedAssets.Libs.CoreLib.Modules.Configuration.Constants;

namespace SharedAssets.Libs.CoreLib.Modules.Configuration
{
    public static class ConfigurationModule
    {
        private static ConfigurationsModel _configurationsModel;

        public static ApplicationConfiguration ApplicationConfiguration
        {
            get { return ConfigurationsModel.GetJsonConfiguration<ApplicationConfiguration>(); }
        }

        public static GameplayConfiguration GameplayConfiguration
        {
            get { return ConfigurationsModel.GetScriptableConfiguration<GameplayConfiguration>(); }
        }


        private static ConfigurationsModel ConfigurationsModel
        {
            get
            {
                if (_configurationsModel == null)
                {
                    _configurationsModel = new ConfigurationsModel();
                    _configurationsModel.LoadJsonConfigurationAndBind<ApplicationConfiguration>(ConfigurationConstants.ApplicationConfigurationFilePath);
                    _configurationsModel.LoadScriptableConfigurationAndBind<GameplayConfiguration>(ConfigurationConstants.GameplayConfigurationFilePath);
                }
                return _configurationsModel;
            }
        }
    }
}