﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SharedAssets.Libs.CoreLib.Modules.Configuration.ConfigurationLib
{
    public class ConfigurationsModel
    {
        private readonly Dictionary<Type, ScriptableObject> _scriptableConfigurationByType = new Dictionary<Type, ScriptableObject>();
        private readonly Dictionary<Type, object> _jsonConfigurationByType = new Dictionary<Type, object>();


        public void LoadScriptableConfigurationAndBind<T>(string pathAtResources) where T : ScriptableObject
        {
            _scriptableConfigurationByType[typeof(T)] = ConfigurationsUtil.LoadScriptableConfiguration<T>(pathAtResources);
        }

        public void LoadJsonConfigurationAndBind<T>(string absolutePath)
        {
            _jsonConfigurationByType[typeof(T)] = ConfigurationsUtil.LoadJsonConfiguration<T>(absolutePath);
        }

        public T GetScriptableConfiguration<T>() where T : ScriptableObject
        {
            return (T)_scriptableConfigurationByType[typeof(T)];
        }

        public T GetJsonConfiguration<T>()
        {
            return (T)_jsonConfigurationByType[typeof(T)];
        }
    }
}

