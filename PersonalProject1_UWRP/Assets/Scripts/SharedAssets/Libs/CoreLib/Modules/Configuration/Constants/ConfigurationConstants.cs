﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace SharedAssets.Libs.CoreLib.Modules.Configuration.Constants
{
    public static class ConfigurationConstants
    {
        public const string JsonPostfix = ".json";
        public const string AssetPostfix = ".asset";

        public const string ApplicationDirectory = "Application";
        public const string ApplicationConfigurationFile = "ApplicationConfiguration" + JsonPostfix;
        public const string GameplayConfigurationFile = "GameplayConfiguration";
        public const string ProjectDirectory = "ProjectConfiguration";
        public const string ConfigurationsDirectory = "Configurations";
        public const string AssetsFolder = "Assets";
        public const string ContentFolder = "/Content";
        public const string EditorFolder = "/Editor";
        public const string IslandGeneratorFolder = "/IslandGenerator";
        public const string IslandGeneratorDataFile = "/IslandGeneratorDataConfig" + AssetPostfix;
        public const string BiomesMapFileName = "BiomesMap";

        public static readonly string ConfigurationsRelativePath = Path.Combine(ProjectDirectory, ConfigurationsDirectory);
        public static readonly string ConfigurationsAtStreamingAssetsPath = Path.Combine(Application.streamingAssetsPath, ConfigurationsRelativePath);
        public static readonly string ApplicationConfigurationsPath = Path.Combine(ConfigurationsAtStreamingAssetsPath, ApplicationDirectory);
        public static readonly string ApplicationConfigurationFilePath = Path.Combine(ApplicationConfigurationsPath, ApplicationConfigurationFile);
        public static readonly string GameplayConfigurationFilePath = Path.Combine(ConfigurationsRelativePath, GameplayConfigurationFile);

        public static readonly string IslandGeneratorDataFilePath = AssetsFolder + ContentFolder + EditorFolder + IslandGeneratorFolder + IslandGeneratorDataFile;
    }
}

