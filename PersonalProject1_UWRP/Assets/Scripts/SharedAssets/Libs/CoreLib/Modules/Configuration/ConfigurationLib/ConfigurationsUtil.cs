﻿using System.IO;
using UnityEngine;

namespace SharedAssets.Libs.CoreLib.Modules.Configuration.ConfigurationLib
{
    public static class ConfigurationsUtil
    {
        public static T LoadScriptableConfiguration<T>(string pathAtResources) where T : ScriptableObject
        {
            return Resources.Load<T>(pathAtResources);
        }

        public static T LoadJsonConfiguration<T>(string absolutePath)
        {
            var json = File.ReadAllText(absolutePath);
            return JsonUtility.FromJson<T>(json);
        }
    }
}
