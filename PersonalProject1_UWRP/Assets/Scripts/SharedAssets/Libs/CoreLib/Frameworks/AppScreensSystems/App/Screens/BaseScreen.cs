﻿using UnityEngine;

namespace SharedAssets.Libs.CoreLib.Frameworks.AppScreensSystems.App.Screens
{
	public abstract class BaseScreen : MonoBehaviour
	{
		public virtual void Show()
		{
			gameObject.SetActive(true);
		}

		public virtual void Hide()
		{
			gameObject.SetActive(false);
		}
	}
}