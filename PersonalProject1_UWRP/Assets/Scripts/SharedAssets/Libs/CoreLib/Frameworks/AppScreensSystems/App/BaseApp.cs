﻿using SharedAssets.Libs.CoreLib.Frameworks.AppScreensSystems.App.Screens;
using UnityEngine;

namespace SharedAssets.Libs.CoreLib.Frameworks.AppScreensSystems.App
{
	public class BaseApp : MonoBehaviour 
	{
		public static BaseScreensManager ScreensManager { get; private set; }
		
		
		[SerializeField] 
		private BaseScreensManager _screensManager;
		
		[SerializeField] 
		protected GameObject _gameObjectSystems;
        [Tooltip("Optional parameter")]
        [SerializeField]
        private GameObject _gameObjectModules;


        protected virtual void Awake()
		{
			InitValues();
            ActivateModules();
            ActivateSystems();
			LaunchApp();
		}
		
		
		protected virtual void InitValues()
		{
			ScreensManager = _screensManager;
		}

		protected virtual void ActivateSystems()
		{
			_gameObjectSystems.SetActive(true);			
		}
        protected virtual void ActivateModules()
        {
            if(_gameObjectModules != null)
            {
                _gameObjectModules.SetActive(true);
            }
            
        }

        protected virtual void LaunchApp()
		{
			_screensManager.Init();
		}
	}
}