﻿using System;
using UnityEngine;

namespace SharedAssets.Libs.CoreLib.Frameworks.AppScreensSystems.App.Screens
{
	public class BaseScreensManager : MonoBehaviour 
	{
		[SerializeField] 
		private GameObject _screensRoot;
		
		
		private BaseScreen[] _screens;


		public void Init()
		{
			_screens = _screensRoot.GetComponentsInChildren<BaseScreen>(true);
		}		

		public T Get<T>() where T : BaseScreen
		{
			return Array.Find(_screens, s => s is T) as T;
		}

		public void HideAll()
		{
			Array.ForEach(_screens, s => s.Hide());
		}
		
		public void ChangeTo<T>(BaseScreen from) where T : BaseScreen
		{
			Change(from, Get<T>());
		}

		public virtual void Change(BaseScreen from, BaseScreen to)
		{
            from.Hide();
            to.Show();
		}
	}
}