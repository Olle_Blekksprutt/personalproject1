﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SharedAssets.Libs.CoreLib.Frameworks.Singleton
{
    public class CrosssceneSingletonComponent<T> : MonoBehaviour where T : Object
    {
        private static T instance;

        public static T Instance => instance;

        private void Awake()
        {
            if (instance == null)
            {
                instance = gameObject.GetComponent<T>();
                DontDestroyOnLoad(gameObject);
            }
            else
            {
                if (instance != this)
                {
                    Destroy(gameObject);
                }

            }



        }
    }
}
