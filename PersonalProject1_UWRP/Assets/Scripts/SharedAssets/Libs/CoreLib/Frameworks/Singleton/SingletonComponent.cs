﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SharedAssets.Libs.CoreLib.Frameworks.Singleton
{
    public class SingletonComponent<T> : MonoBehaviour where T: Object
    {
        private static T instance;

        public static T Instance => instance;

        private void Awake()
        {
            instance = gameObject.GetComponent<T>();
        }
    }
}

