﻿using System.Collections;
using System.Collections.Generic;
using GarlicTech.Strategy1.Systems.PlayerInput;
using GarlicTech.Strategy1.Systems.UnitControl;
using SharedAssets.Libs.CoreLib.Frameworks.AppScreensSystems.App.Screens;
using UnityEngine;

namespace GarlicTech.Strategy1.Screens.Game
{
    public class GameScreen : BaseScreen
    {
        private void OnEnable()
        {
            PlayerInputSystem.Instance.ObjectDeselectedEvent += UnitControlSystem.Instance.OnDeselectHandler;
        }

        private void OnDisable()
        {
            PlayerInputSystem.Instance.ObjectDeselectedEvent -= UnitControlSystem.Instance.OnDeselectHandler;
        }
    }
}

