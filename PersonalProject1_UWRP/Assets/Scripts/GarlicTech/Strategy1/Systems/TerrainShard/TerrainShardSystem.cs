﻿using System.Collections;
using System.Collections.Generic;
using GarlicTech.Strategy1.Systems.TerrainShard.Subsystems;
using GarlicTechShared.Core.Framework;
using GarlicTechShared.Modules.ErrorHandling;
using UnityEngine;

namespace GarlicTech.Strategy1.Systems.TerrainShard
{
    public class TerrainShardSystem : SingletonComponent<TerrainShardSystem>, IInitiable
    {
        private TerrainShardController terrainShardController;
        public TerrainShardController TerrainShardController => terrainShardController;

        public void Initialize()
        {
            terrainShardController = gameObject.GetComponentInChildren<TerrainShardController>();
            if(terrainShardController == null)
            {
                ErrorHandlingModule.HandleError(HandledErrorType.InitializationError, this.gameObject, "terrainShardController is null");
            }
        }
    }
}
