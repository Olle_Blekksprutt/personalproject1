﻿using System.Collections;
using System.Collections.Generic;
using GarlicTechShared.Core.Framework;
using UnityEngine;

namespace GarlicTech.Strategy1.Systems.TerrainShard.Entity
{
    public class TerrainShardEntity : MonoBehaviour
    {
        [SerializeField]
        private int shardNumber;
        public int ShardNumber => shardNumber;
    }
}
