﻿using System.Collections;
using System.Collections.Generic;
using GarlicTechShared.Core.Framework;
using UnityEngine;
using System;
using GarlicTech.Strategy1.Systems.PlayerInput.Tools;
using GarlicTech.Strategy1.Systems.UnitControl.Entity;
using GarlicTech.Strategy1.Systems.UnitControl;

namespace GarlicTech.Strategy1.Systems.PlayerInput
{
    public class PlayerInputSystem : SingletonComponent<PlayerInputSystem>, IInitiable
    {
        [SerializeField]
        private LayerMask selectableLayer;
        [SerializeField]
        private float minDistanceToDrawSelection;
        [SerializeField]
        private List<string> tagsToSelect = new List<string>();
        [SerializeField]
        private List<string> tagsToDeselect = new List<string>();
        [SerializeField]
        private Color32 rectColor = new Color32(255, 255, 255, 64);
        [SerializeField]
        private Color32 rectBorderColor = new Color32(255, 255, 255, 255);

        private bool isSelecting;
        private bool isSingleSelectSuccess;
        private Vector3 startSelectingPosition;
        private Vector3 endSelectingPosition;

        public event Action ObjectDeselectedEvent = () => { };

        public void Initialize()
        {
            isSelecting = false;
        }

        private void Update()
        {
            CheckForMouseLeftClick();
            CheckForMouseRightClick();
            CheckForHotkeysSelection();
        }

        private void CheckForMouseLeftClick()
        {
            if(Input.GetMouseButtonDown(0))
            {
                isSingleSelectSuccess = false;
                isSelecting = true;

                startSelectingPosition = Input.mousePosition;
                Ray camRay = Camera.main.ScreenPointToRay(Input.mousePosition);
                RaycastHit hit;
                if (Physics.Raycast(camRay, out hit, 100f, selectableLayer))
                {
                    bool isCompare = false;
                    foreach(string tag in tagsToSelect)
                    {
                        if (hit.transform.CompareTag(tag))
                        {
                            isCompare = true;
                        }
                    }
                    if(isCompare)
                    {
                        isSingleSelectSuccess = true;
                        UnitEntity unitEntity = hit.transform.gameObject.GetComponent<UnitEntity>();
                        List<UnitEntity> selectedObjects = new List<UnitEntity>();
                        selectedObjects.Add(unitEntity);
                        UnitControlSystem.Instance.SelectObjects(selectedObjects);
                    }
                    else
                    {
                        ObjectDeselectedEvent.Invoke();
                    }
                }
            }

            if (Input.GetMouseButtonUp(0))
            {
                CalculateSelection();
                isSelecting = false;
            }
        }

        private void OnGUI()
        {
            if (isSelecting)
            {
                Rect rect = ScreenHelper.GetScreenRect(startSelectingPosition, Input.mousePosition);
                ScreenHelper.DrawScreenRect(rect, rectColor);
                ScreenHelper.DrawScreenRectBorder(rect, 1.0f, rectBorderColor);
            }
        }

        private void CalculateSelection()
        {
            List<UnitEntity> selectedObjects = new List<UnitEntity>();
            foreach(UnitEntity element in UnitControlSystem.Instance.VisibleUnitsList)
            {
                if(IsWithinSelectionBounds(element.transform))
                {
                    selectedObjects.Add(element);
                }
            }

            if(isSingleSelectSuccess && selectedObjects.Count == 0)
            {

            }
            else
            {
                UnitControlSystem.Instance.SelectObjects(selectedObjects);
            }


        }

        private void CheckForMouseRightClick()
        {

        }

        private void CheckForHotkeysSelection()
        {
            bool isNumberPressed = false;
            int numberPressed = 0;

            if (Input.GetKeyDown(KeyCode.Alpha0))
            {
                isNumberPressed = true;
                numberPressed = 0;
            }
            if (Input.GetKeyDown(KeyCode.Alpha1))
            {
                isNumberPressed = true;
                numberPressed = 1;
            }
            if (Input.GetKeyDown(KeyCode.Alpha2))
            {
                isNumberPressed = true;
                numberPressed = 2;
            }

            if (Input.GetKeyDown(KeyCode.Alpha3))
            {
                isNumberPressed = true;
                numberPressed = 3;
            }
            if (Input.GetKeyDown(KeyCode.Alpha4))
            {
                isNumberPressed = true;
                numberPressed = 4;
            }
            if (Input.GetKeyDown(KeyCode.Alpha5))
            {
                isNumberPressed = true;
                numberPressed = 5;
            }
            if (Input.GetKeyDown(KeyCode.Alpha6))
            {
                isNumberPressed = true;
                numberPressed = 6;
            }
            if (Input.GetKeyDown(KeyCode.Alpha7))
            {
                isNumberPressed = true;
                numberPressed = 7;
            }
            if (Input.GetKeyDown(KeyCode.Alpha8))
            {
                isNumberPressed = true;
                numberPressed = 8;
            }
            if (Input.GetKeyDown(KeyCode.Alpha9))
            {
                isNumberPressed = true;
                numberPressed = 9;
            }
            if(isNumberPressed)
            {
                if (Input.GetKey(KeyCode.LeftControl) || Input.GetKey(KeyCode.RightControl))
                {
                    UnitControlSystem.Instance.AddOrChangeHotkeySelectionForNumber(numberPressed);
                }
                else
                {
                    UnitControlSystem.Instance.ApplySelectionGroup(numberPressed);
                    
                }
            }

 
        }

        private bool IsWithinSelectionBounds(Transform transform)
        {
            bool result = false;
            if(isSelecting == false)
            {
                result = false;
            }
            else
            {
                Bounds viewportBounds = ScreenHelper.GetViewportBounds(Camera.main, startSelectingPosition, Input.mousePosition);
                result = viewportBounds.Contains(Camera.main.WorldToViewportPoint(transform.position));
            }

            return result;
        }




    }
}

