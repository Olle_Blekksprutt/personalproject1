﻿using System.Collections;
using System.Collections.Generic;
using GarlicTech.Strategy1.Systems.UnitControl.Entity;
using UnityEngine;
using System;

namespace GarlicTech.Strategy1.Systems.UnitControl.Data
{
    [Serializable]
    public class HotkeySelection
    {
        public int NumberKey;
        private List<UnitEntity> storedSelection;
        public List<UnitEntity> StoredSelection
        {
            get
            {
                List<UnitEntity> newSelectionList = new List<UnitEntity>();
                foreach(UnitEntity element in storedSelection)
                {
                    if(element != null && element.gameObject.activeInHierarchy)
                    {
                        newSelectionList.Add(element);
                    }
                }
                storedSelection = newSelectionList;

                return storedSelection;
            }
        }
        

        public HotkeySelection()
        {
            NumberKey = 0;
            storedSelection = new List<UnitEntity>();
        }

        public HotkeySelection(int NumberKey, List<UnitEntity> StoredSelection)
        {
            this.NumberKey = NumberKey;
            this.storedSelection = StoredSelection;
        }

        public void ChangeSelectedGroup(List<UnitEntity> newStoredSelection)
        {
            this.storedSelection = newStoredSelection;
        }
    }
}

