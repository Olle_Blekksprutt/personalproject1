﻿using System.Collections;
using System.Collections.Generic;
using GarlicTech.Strategy1.Data.Enumerations;
using GarlicTech.Strategy1.Systems.UnitControl.Data;
using GarlicTech.Strategy1.Systems.UnitControl.Entity;
using GarlicTechShared.Core.Framework;
using GarlicTechShared.Modules.ErrorHandling;
using UnityEngine;

namespace GarlicTech.Strategy1.Systems.UnitControl
{
    public class UnitControlSystem : SingletonComponent<UnitControlSystem>, IInitiable
    {
        [SerializeField]
        private List<UnitEntity> visibleUnits;
        [SerializeField]
        private List<UnitEntity> selectedUnits;
        [SerializeField]
        private List<HotkeySelection> hotkeysSelections; // need to store this

        public List<UnitEntity> VisibleUnitsList => visibleUnits;


        public void Initialize()
        {
            hotkeysSelections = new List<HotkeySelection>();
            visibleUnits = new List<UnitEntity>();
            selectedUnits = new List<UnitEntity>();
        }

        public void RegisterVisibleUnitsList(UnitEntity affectedObject, bool isVisible)
        {
            if (isVisible)
            {
                if (visibleUnits.Contains(affectedObject) == false)
                {
                    visibleUnits.Add(affectedObject);
                }
            }
            else
            {
                if (visibleUnits.Contains(affectedObject))
                {
                    visibleUnits.Add(affectedObject);
                }
            }
        }

        public void SelectObjects(List<UnitEntity> selectedObjects)
        {
            List<UnitEntity> selectedRearrangeObjects = new List<UnitEntity>();
            if (selectedObjects.Count > 1)
            {
                foreach(UnitEntity element in selectedObjects)
                {
                    if(element.CurrentUnitType == UnitGlobalType.PlayerUnit)
                    {
                        selectedRearrangeObjects.Add(element);
                    }
                }
            }
            else
            {
                selectedRearrangeObjects = selectedObjects;
            }

            this.selectedUnits = selectedRearrangeObjects;
        }

        public void OnDeselectHandler()
        {
            selectedUnits = new List<UnitEntity>();
        }

        public void AddOrChangeHotkeySelectionForNumber(int number)
        {
            bool needToAdd = true;
            int numberIndex = 0;
            for(int i = 0; i < hotkeysSelections.Count; i++)
            {
                if(hotkeysSelections[i].NumberKey == number)
                {
                    numberIndex = i;
                    needToAdd = false;
                }
            }
            if(needToAdd)
            {
                HotkeySelection newHotkeySelection = new HotkeySelection(number, selectedUnits);
                hotkeysSelections.Add(newHotkeySelection);
            }
            else
            {
                hotkeysSelections[numberIndex].ChangeSelectedGroup(selectedUnits);
            }


        }

        public void ApplySelectionGroup(int number)
        {
            bool finded = false;
            int numberIndex = 0;
            for (int i = 0; i < hotkeysSelections.Count; i++)
            {
                if (hotkeysSelections[i].NumberKey == number)
                {
                    numberIndex = i;
                    finded = true;
                }
            }
            if(finded)
            {
                selectedUnits = hotkeysSelections[numberIndex].StoredSelection;
            }
        }
    }
}
