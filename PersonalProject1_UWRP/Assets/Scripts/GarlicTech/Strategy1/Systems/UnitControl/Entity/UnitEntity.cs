﻿using System;
using System.Collections;
using System.Collections.Generic;
using GarlicTech.Strategy1.Data.Enumerations;
using GarlicTech.Strategy1.Systems.UnitControl.Components;
using GarlicTechShared.Modules.ErrorHandling;
using UnityEngine;

namespace GarlicTech.Strategy1.Systems.UnitControl.Entity
{
    [Serializable]
    public class UnitEntity : MonoBehaviour, IComparable<UnitEntity>
    {
        [SerializeField]
        private UnitGlobalType currentUnitType;
        private List<OnVisibleReactionComponent> objectVisualReactionComponents = new List<OnVisibleReactionComponent>();
        bool isObjectVisible;
        private System.Guid unitGUID;

        public System.Guid UnitGUID => unitGUID;
        public UnitGlobalType CurrentUnitType => currentUnitType;

        private void Awake()
        {
            unitGUID = System.Guid.NewGuid();
            isObjectVisible = false;
            OnVisibleReactionComponent[] visualReactionComponentsMass = gameObject.GetComponentsInChildren<OnVisibleReactionComponent>();
            if(visualReactionComponentsMass != null && visualReactionComponentsMass.Length >0)
            {
                foreach (OnVisibleReactionComponent element in visualReactionComponentsMass)
                {
                    element.objectVisibleChangedEvent += HandleObjectVisibleHandler;
                    objectVisualReactionComponents.Add(element);
                }
            }
            else
            {
                ErrorHandlingModule.HandleError(HandledErrorType.UnitSetupError, gameObject, "OnVisibleReactionComponent not added");
            }

        }
        private void OnDestroy()
        {
            foreach (OnVisibleReactionComponent element in objectVisualReactionComponents)
            {
                element.objectVisibleChangedEvent -= HandleObjectVisibleHandler;
            }
        }

        private void HandleObjectVisibleHandler(bool isVisible)
        {
            isObjectVisible = false;
            foreach (OnVisibleReactionComponent element in objectVisualReactionComponents)
            {
                if(element.IsVisible)
                {
                    isObjectVisible = true;
                }
            }

            UnitControlSystem.Instance.RegisterVisibleUnitsList(this, isObjectVisible);
        }

        public int CompareTo(UnitEntity other)
        {
            return unitGUID.CompareTo(other.UnitGUID);
        }
    }
}
