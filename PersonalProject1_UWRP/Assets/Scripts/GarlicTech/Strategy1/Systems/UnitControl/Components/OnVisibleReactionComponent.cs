﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace GarlicTech.Strategy1.Systems.UnitControl.Components
{
    [Serializable]
    public class OnVisibleReactionComponent : MonoBehaviour
    {
        private bool isVisible;
        public Action<bool> objectVisibleChangedEvent = (isVisible) => { };
        public bool IsVisible => isVisible;

        void OnBecameInvisible()
        {
            isVisible = false;
            objectVisibleChangedEvent.Invoke(isVisible);
        }

        void OnBecameVisible()
        {
            isVisible = true;
            objectVisibleChangedEvent.Invoke(isVisible);
        }
    }
}
