﻿using System;

namespace GarlicTech.Strategy1.Data.Enumerations
{
    [Serializable]
    public enum UnitGlobalType
    {
        PlayerUnit = 0,
        EnemyUnit = 1,
        Building = 2
    }
}

