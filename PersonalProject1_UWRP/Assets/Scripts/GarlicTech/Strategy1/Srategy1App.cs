﻿using System.Collections;
using System.Collections.Generic;
using GarlicTech.Strategy1.Screens.Game;
using GarlicTechShared.Core.Framework;
using SharedAssets.Libs.CoreLib.Frameworks.AppScreensSystems.App;
using UnityEngine;

namespace GarlicTech.Strategy1
{
    public class Srategy1App : BaseApp
    {
        protected override void ActivateSystems()
        {
            base.ActivateSystems();
            IInitiable[] systemsToInit = _gameObjectSystems.GetComponentsInChildren<IInitiable>();
            if(systemsToInit != null && systemsToInit.Length > 0)
            {
                foreach (IInitiable element in systemsToInit)
                {
                    element.Initialize();
                }
            }


        }

        protected override void LaunchApp()
        {
            base.LaunchApp();
            ScreensManager.Get<GameScreen>().Show();
        }
    }
}

