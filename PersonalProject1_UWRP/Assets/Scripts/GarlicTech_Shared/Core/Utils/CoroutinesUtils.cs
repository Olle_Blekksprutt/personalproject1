﻿using System.Collections;
using UnityEngine;
using System;
using GarlicTech_SharedAssets.Modules.MainThreadExecutor;

namespace GarlicTechShared.Core.Utils
{
    public static class CoroutinesUtils
    {
        public static void CallAfter(float seconds,  Action action, MonoBehaviour coroutineHolder = null)
        {
            MonoBehaviour currentCoroutineHolder = coroutineHolder;
            if(currentCoroutineHolder == null)
            {
                currentCoroutineHolder = MainThreadExecutorModule.Instance as MonoBehaviour;
            }
            if(currentCoroutineHolder != null)
            {
                currentCoroutineHolder.StartCoroutine(CallAfterCoroutine(seconds, action));
            }
            else
            {
                Debug.LogError("No holder for coroutine execution");
            }

        }

        public static void CallAfterFrames(int numFrames, Action action, MonoBehaviour coroutineHolder = null)
        {
            MonoBehaviour currentCoroutineHolder = coroutineHolder;
            if (currentCoroutineHolder == null)
            {
                currentCoroutineHolder = MainThreadExecutorModule.Instance as MonoBehaviour;
            }
            if (currentCoroutineHolder != null)
            {
                currentCoroutineHolder.StartCoroutine(CallAfterFramesCoroutine(numFrames, action));
            }
            else
            {
                Debug.LogError("No holder for coroutine execution");
            }
        }


        private static IEnumerator CallAfterCoroutine(float seconds, Action action)
        {
            yield return new WaitForSeconds(seconds);
            action();
        }

        private static IEnumerator CallAfterFramesCoroutine(int numFrames, Action action)
        {
            for (var i = 0; i < numFrames; i++)
            {
                yield return null;
            }
            action();
        }
    }
}

