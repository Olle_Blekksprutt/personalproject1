﻿using System;
using UnityEngine.Events;

namespace GarlicTechShared.Core.Utils
{
    public static class DispatchUtil
    {
        public static void DispatchIfNotNull(Action action)
        {
            if (action != null)
            {
                action();
            }
        }

        public static void DispatchIfNotNull<T>(Action<T> action, T obj)
        {
            if (action != null)
            {
                action(obj);
            }
        }

        public static void DispatchIfNotNull<T, U>(Action<T, U> action, T t, U u)
        {
            if (action != null)
            {
                action(t, u);
            }
        }

        public static void DispatchIfNotNull(UnityEvent unityEvent)
        {
            if (unityEvent != null)
            {
                unityEvent.Invoke();
            }
        }

        public static void DispatchIfNotNull<T>(UnityEvent<T> unityEvent, T t)
        {
            if (unityEvent != null)
            {
                unityEvent.Invoke(t);
            }
        }
    }
}

