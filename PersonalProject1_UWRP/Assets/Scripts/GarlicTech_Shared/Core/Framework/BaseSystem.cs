﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GarlicTechShared.Core.Framework
{
    public abstract class BaseSystem<T> : MonoBehaviour where T : Object
    {
        public abstract void InitializeSystem();

    }
}

