﻿using System;

namespace GarlicTechShared.Core.Framework
{
    public interface IInitiable
    {
        void Initialize();
    }
}

