﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GarlicTechShared.Core.Framework
{
    public abstract class SingletonComponent<T> : MonoBehaviour where T : Object
    {
        private static T instance;
        public static T Instance => instance;

        private void Awake()
        {
            instance = gameObject.GetComponent<T>();
        }
        public static bool Exists()
        {
            return instance != null;
        }
    }
}
