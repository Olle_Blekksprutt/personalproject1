﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FirstGameShared.App.Modules.GameplayData.Enumerations
{
    public enum MainWindowState
    {
        MainMenu,
        SettingsMenu
    }

    [Serializable]
    public enum BiomeTypes
    {
        Water,
        Beach,
        Ground,
        Rock,
        Mountain,
        Reef
    }
}