﻿using System.Collections;
using System.Collections.Generic;
using GarlicTech_SharedAssets.App.Modules.SaveLoad;
using UnityEngine;

public class TestSaveLoad : MonoBehaviour
{
    Obj obj;
    Obj loaded;
    void Start()
    {
        obj = new Obj();
        obj.s = "test";
        obj.n = 0;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.S))
        { 
            SaveLoadModule.Save(obj);
        }
        
        if (Input.GetKeyDown(KeyCode.L))
        {
            loaded = SaveLoadModule.Load<Obj>();
        }
    }
}

public class Obj : ISaveable
{
    public string s;
    public int n;
}
