﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEngine;

namespace GarlicTech_SharedAssets.App.Modules.SaveLoad
{
    public class SaveLoadModule
    {
        public static T Load<T>() where T : ISaveable
        {
            string name = typeof(T).ToString();
            string savedName = Base64Encode(name);
            string folderPath = Application.persistentDataPath;
            string path = Path.Combine(folderPath, savedName);
            string text = File.ReadAllText(path);
            string loadedText = Base64Decode(text);
            
            return (T)JsonUtility.FromJson(loadedText, typeof(T));
        }

        public static void Save<T>(T instance) where T : ISaveable
        {
            string name = typeof(T).ToString();
            string text = JsonUtility.ToJson(instance);
            string textToSave = Base64Encode(text);
            string nameToSave = Base64Encode(name);
            string folderPath = Application.persistentDataPath;
            string path = Path.Combine(folderPath, nameToSave);

            File.WriteAllText(path, textToSave);
        }

        public static void SaveMatrix(string text, string name) 
        {
            string folderPath = Application.streamingAssetsPath;
            string path = Path.Combine(folderPath, name);

            File.WriteAllText(path, text);
        }

        public static string[] LoadMatrix(string name)
        {
            string folderPath = Application.streamingAssetsPath;
            string path = Path.Combine(folderPath, name);
            string[] text = File.ReadAllLines(path);

            return text;
        }

        static string Base64Decode(string base64EncodedData)
        {
#if !UNITY_EDITOR
        byte[] base64EncodedBytes = Convert.FromBase64String(base64EncodedData);
        return Encoding.UTF8.GetString(base64EncodedBytes);
#elif UNITY_EDITOR
            return base64EncodedData;
#endif
        }

        static string Base64Encode(string plainText)
        {
#if !UNITY_EDITOR
        byte[] plainTextBytes = Encoding.UTF8.GetBytes(plainText);
        return Convert.ToBase64String(plainTextBytes);
#elif UNITY_EDITOR
            return plainText;
#endif
        }
    }
}