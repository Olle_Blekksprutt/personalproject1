﻿using System;
using System.Collections;
using System.Collections.Generic;
using SharedAssets.Libs.CoreLib.Frameworks.Singleton;
using UnityEngine;

namespace GarlicTech_SharedAssets.Modules.MainThreadExecutor
{
    public class MainThreadExecutorModule : MonoBehaviour, IMainThreadExecutor
    {
        private static MainThreadExecutorModule instance;
        public static MainThreadExecutorModule Instance
        {
            get
            {
                if (instance == null)
                {
                    GameObject MainThreadExecutorModuleObject = new GameObject("MainThreadExecutorModuleObject");
                    MainThreadExecutorModuleObject.AddComponent<MainThreadExecutorModule>();
                    instance = MainThreadExecutorModuleObject.GetComponent<MainThreadExecutorModule>();
                }

                return instance;
            }
        }

        private void OnEnable()
        {
            if (instance == null)
            {
                instance = this;
            }
        }
        public MonoBehaviour MainCoroutinesExecutor
        {
            get
            {
                return this;
            }
        }

        private readonly List<Action> _actions = new List<Action>();


        void IMainThreadExecutor.AddToExecute(Action action)
        {
            _actions.Add(action);
        }

        private void Update()
        {
            _actions.ForEach(a => a.Invoke());
            _actions.Clear();
        }
    }

    interface IMainThreadExecutor 
    {
        void AddToExecute(Action action);
    }

}

