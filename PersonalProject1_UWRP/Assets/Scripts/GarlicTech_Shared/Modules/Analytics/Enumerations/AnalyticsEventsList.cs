﻿using System;

namespace GarlicTech_SharedAssets.Modules.AppAnalytics.Enumerations
{
    [Serializable]
    public enum AnalyticsEventsList
    {
        GameStarted,
        GameEnded,
        GameLevelStarted,
        GameLevelEnded,
        StoreOpened,
        BuyButtonPressed,
        BuyOperationSuccess,
        NuBuyOperationFailed,
        StoreClosed,
        DailyBonusAwarded,
        InGameItemSelled,
        ApplicationError
    }
}

