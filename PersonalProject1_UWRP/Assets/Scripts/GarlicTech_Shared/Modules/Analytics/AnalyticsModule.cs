﻿using System.Collections;
using System.Collections.Generic;
using GarlicTech_SharedAssets.Modules.AppAnalytics.Events;
using UnityEngine;
using UnityEngine.Analytics;


namespace GarlicTech_SharedAssets.Modules.AppAnalytics
{
    public class AnalyticsModule : MonoBehaviour
    {

        private void Awake()
        {
            DontDestroyOnLoad(this);
        }
        public void SentAnalyticsEvent(BaseEvent customEvent)
        {
            Analytics.CustomEvent(customEvent.AnalyticsEventId.ToString(), customEvent.ReturnAnalyticsEventData());
        }


    }
}

