﻿using System.Collections;
using System.Collections.Generic;
using GarlicTech_SharedAssets.Modules.AppAnalytics.Enumerations;
using UnityEngine;

namespace GarlicTech_SharedAssets.Modules.AppAnalytics.Events
{
    public class ApplicationStartedEvent : BaseEvent
    {
        private AnalyticsEventsList eventType;
        public override AnalyticsEventsList AnalyticsEventId => eventType;

        private void Awake()
        {

        }

        public override Dictionary<string, object> ReturnAnalyticsEventData()
        {
            Dictionary<string, object> result = new Dictionary<string, object>();

            return result;
        }
    }
}
