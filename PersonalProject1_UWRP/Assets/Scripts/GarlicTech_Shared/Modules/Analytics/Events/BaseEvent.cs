﻿using System.Collections;
using System.Collections.Generic;
using GarlicTech_SharedAssets.Modules.AppAnalytics.Enumerations;
using UnityEngine;

namespace GarlicTech_SharedAssets.Modules.AppAnalytics.Events
{
    public abstract class BaseEvent: MonoBehaviour
    {
        protected AnalyticsEventsList analyticsEventId;
        public abstract AnalyticsEventsList AnalyticsEventId { get; }

        public abstract Dictionary<string, object> ReturnAnalyticsEventData();
    }
}
