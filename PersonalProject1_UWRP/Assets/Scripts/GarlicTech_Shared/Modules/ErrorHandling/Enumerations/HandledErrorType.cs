﻿using System;

namespace GarlicTechShared.Modules.ErrorHandling
{
    [Serializable]
    public enum HandledErrorType
    {
        InitializationError = 0,
        UnitSetupError = 1
    }
}
