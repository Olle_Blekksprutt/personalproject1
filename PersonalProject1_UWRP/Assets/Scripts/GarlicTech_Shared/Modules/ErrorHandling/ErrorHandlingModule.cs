﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace GarlicTechShared.Modules.ErrorHandling
{
    public static class ErrorHandlingModule
    {
        public static void HandleError(HandledErrorType errorType, GameObject gameObjectWithProblem, string additionalText = "", object problemObject = null)
        {
            switch(errorType)
            {
                case HandledErrorType.InitializationError:
                    Debug.LogError(String.Format("Error {3} in initialization in {0}, {1} appeared in {2}", gameObjectWithProblem.name, additionalText, problemObject?.GetType().ToString(), GetFullErrorCaption(errorType)), gameObjectWithProblem);
                    Application.Quit();
                    break;
                case HandledErrorType.UnitSetupError:
                    Debug.LogError(String.Format("Error {3} in object setup for {0}, {1} appeared in {2}", gameObjectWithProblem.name, additionalText, problemObject?.GetType().ToString(), GetFullErrorCaption(errorType)), gameObjectWithProblem);
                    Application.Quit();
                    break;
                default:
                    Debug.LogError("UNHANDLED ERROR! CHECK THE MODULE TO ADD THE NADLE BEHAVIOUR");
                    break;
            }
        }

        private static string GetFullErrorCaption(HandledErrorType errorType)
        {
            return ((int)errorType) + " - " + errorType.ToString();
        }
    }


}

